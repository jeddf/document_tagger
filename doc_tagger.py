import re
import sys
import os
 
# Define Regex expressions for metadata searches!
textpath = sys.argv[1] 
title_search = re.compile(r'''
    (?:title:        # Look for title leader
    \s*)           # followed by 0+ whitespace chars
    (?P<title>    # capture following in group named title
    (             # 
      (
        \S+       # 1+ non whitespace characters
        (\ )?     # Followed by 0 or 1 whitespace chars (The backslash escapes the whitespace character for regex because VERBOSE mode ignores em.
      )+          # Title will have 1 or more of these^ words
    )
    (
      (\n(\ )+)   # optional new line
      (\S*(\ )?)* # followed by 0+ words
    )*            # to handle as many lines as required
    )''',
    re.IGNORECASE | re.VERBOSE) # Pipe for multiple flags for regex!
author_search = re.compile(r'''
    author:           #Find author leader
    (?P<author>.*)    #Saved group named translator containing 0+ any chars
    ''',
    re.IGNORECASE | re.VERBOSE)
translator_search = re.compile('''
    translator:        #Find translator leader
    (?P<translator>    #saved group named translator
    .*)                #containing 0+ any chars
    ''',
    re.IGNORECASE | re.VERBOSE)
illustrator_search = re.compile('''
    illustrator:       #Find illustrator leader
    (?P<illustrator>   #saved group named illustrator
    .*)                # Containint 0+ any char's
    ''',
    re.IGNORECASE | re.VERBOSE)
kws = {kw: re.compile(r'\b' + kw + r'\b') for kw in sys.argv[2:]}    # Dictionary comprehension!
# List filenames in user specified directory, for each one, if it's .txt: save contents in variable and parse for metadata, search for user speicifed keywords.
for fl in (os.listdir(textpath)):
    if fl.endswith('.txt'):
        fl_path = os.path.join(textpath, fl)
        with open(fl_path) as f:
            doc = f.read()
        print '***'*25
        title =  re.search(title_search, doc).group('title')
        author = re.search(author_search, doc)
        if author:
            author = author.group('author')
            translator = re.search(translator_search, doc)
        if translator:
            translator = translator.group('translator')
        print 'Here\'s the info for file {}: \n'.format(fl)
        print 'Title: {}'.format(title)
        print 'Author: {}'.format(author)
        print 'Translator: {}'.format(translator)
        print '\n---Keyword Results---'
        strt = 0
        end = -1
        if '***' in doc:
            strt = doc.index('RT***')
            end = doc.index('***END')
        for kw in kws:
            print '{0}:{1} '.format(kw, len(re.findall(kws[kw], doc[strt:end])))
        print '\n'
