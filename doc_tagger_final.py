''' Gutenberg crawler thinkful project, inefficient solution mainly due to unneccessary overuse of re.compile'''
import sys
import re
import os

def txt_paths(path):
    txt_paths = []
    for fl in (os.listdir(path)):
        if fl.endswith('.txt'):
            txt_paths.append(os.path.join(path, fl))
    return txt_paths

def main(txt_paths, kws):
    for fl in txt_paths:
        with open(fl, 'r') as f:
            full_txt = f.read()
            title(full_txt)
            author(full_txt)
            translator(full_txt)
            keywords(full_txt, kws)

def title(full_txt):
    title = re.search(re.compile(r'(?:title:\s*)(?P<title>((\S*( )?)+)' + r'((\n(\ )+)(\S*(\ )?)*)*)', re.IGNORECASE), full_txt)
    print "\nTitle: " + title.group('title') 

def author(full_txt):
    author = re.search(re.compile(r'(author:)(?P<author>.*)', re.IGNORECASE), full_txt)
    if author:
        print "Author: " + author.group('author')

def translator(full_txt):
    translator = re.search(re.compile(r'(translator:)(?P<translator>.*)', re.IGNORECASE), full_txt)
    if translator:
        print "Translator: " + translator.group('translator')

def keywords(full_txt, kws):
    print "****" * 25
    for kw in kws:
        print "\"{0}\": {1}".format(kw, len(re.findall(re.compile(r'\b' + kw + r'\b'), full_txt)))
txt_paths = txt_paths(sys.argv[1])
print txt_paths
kws = sys.argv[2:]
if __name__ == '__main__':
    main()
