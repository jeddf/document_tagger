import re
import sys
from pg_sample_texts import DIV_COMM, MAG_CART

documents = [DIV_COMM, MAG_CART]

# Define Regex expressions for metadata searches!

searches = {}
for kw in sys.argv[1:]:
    searches[kw] = re.compile(r"\b" + kw + r"\b", re.IGNORECASE)

title_search = re.compile(r'''
    (?:title:        # Look for title leader
    \s*)           # followed by 0+ whitespace chars
    (?P<title>    # capture following in group named title
    (             # 
      (
        \S+       # 1+ non whitespace characters
        (\ )?     # Followed by 0 or 1 whitespace chars (The backslash escapes the whitespace character for regex because VERBOSE mode ignores em.
      )+          # Title will have 1 or more of these^ words
    )
    (
      (\n(\ )+)   # optional new line
      (\S*(\ )?)* # followed by 0+ words
    )*            # to handle as many lines as required
    )''',
    re.IGNORECASE | re.VERBOSE) # Pipe for multiple flags for regex!
author_search = re.compile(r'''
    author:           #Find author leader
    (?P<author>.*)    #Saved group named translator containing 0+ any chars
    ''',
    re.IGNORECASE | re.VERBOSE)
translator_search = re.compile('''
    translator:        #Find translator leader
    (?P<translator>    #saved group named translator
    .*)                #containing 0+ any chars
    ''',
    re.IGNORECASE | re.VERBOSE)
illustrator_search = re.compile('''
    illustrator:       #Find illustrator leader
    (?P<illustrator>   #saved group named illustrator
    .*)                # Containint 0+ any char's
    ''',
    re.IGNORECASE | re.VERBOSE)

for i, doc in enumerate(documents):
    print '***'*25
    title =  re.search(title_search, doc).group('title')
    author = re.search(author_search, doc)
    if author:
        author = author.group('author')
    translator = re.search(translator_search, doc)
    if translator:
        translator = translator.group('translator')
    illustrator = re.search(illustrator_search, doc)
    if illustrator:
        illustrator = illustrator.group('illustrator')
    print 'Here\'s the info for doc {}: \n'.format(i)
    print 'Title: {}'.format(title)
    print 'Author: {}'.format(author)
    print 'Translator: {}'.format(translator)
    print 'Illustrator: {}'.format(illustrator)
    print '\n---Keyword Results---'
    if searches:
        strt = 0
        end = -1
        if '***\n' in doc:
            strt = doc.index(' ***\n')
            end = doc.index('*** END')
        for kw in searches:
            print '{}: '.format(kw), len(re.findall(searches[kw], doc[strt:end]))
    print '\n'
